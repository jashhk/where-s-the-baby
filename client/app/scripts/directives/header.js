'use strict';

/**
 * @ngdoc directive
 * @name loopbackApp.directive:navbar
 * @description
 * # navbar
 */
angular.module('loopbackApp')
  .directive('header', function () {
    return {
      templateUrl: 'views/elements/header.html',
      restrict: 'E'
    };
  });
