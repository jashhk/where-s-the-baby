'use strict';

/**
 * @ngdoc directive
 * @name loopbackApp.directive:sidebar
 * @description
 * # sidebar
 */
angular.module('loopbackApp')
  .directive('sidebar', function ($cookieStore, $window) {
    return {
      templateUrl: 'views/elements/sidebar.html',
      restrict: 'AE',
      link: function ($scope, element, attrs) {
        /**
         * Sidebar Toggle & Cookie Control
         *
         */
        var mobileView = 992;

        $scope.getWidth = function () {
          return $window.innerWidth;
        };

        $scope.$watch($scope.getWidth, function (newValue, oldValue) {
          if (newValue >= mobileView) {
            if (angular.isDefined($cookieStore.get('toggle'))) {
              if ($cookieStore.get('toggle') == false) {
                $scope.toggle = false;
              } else {
                $scope.toggle = true;
              }
            } else {
              $scope.toggle = true;
            }
          } else {
            $scope.toggle = false;
          }

        });

        $window.onresize = function () {
          $scope.$apply();
        };

        $scope.toggleSidebar = function () {
          $scope.toggle = !$scope.toggle;

          $cookieStore.put('toggle', $scope.toggle);
        };
      }
    };
  });
